# st - simple terminal
# See LICENSE file for copyright and license details.
.POSIX:

include config.mk

SRC = st.c x.c hb.c
OBJ = $(SRC:.c=.o)

all: options st

options:
	@echo st build options:
	@echo "CFLAGS  = $(STCFLAGS)"
	@echo "LDFLAGS = $(STLDFLAGS)"
	@echo "CC      = $(CC)"
	@echo

config.h:
	cp config.def.h config.h

.c.o:
	$(CC) $(STCFLAGS) -c $<

st.o: config.h st.h win.h
x.o: arg.h config.h st.h win.h hb.h
hb.o: st.h

$(OBJ): config.h config.mk

st: $(OBJ)
	$(CC) -o $@ $(OBJ) $(STLDFLAGS)

clean:
	rm -f st $(OBJ) st-mira-$(VERSION).tar.gz
	rm -f *.{rej,orig}

dist:
	mkdir -p st-mira-$(VERSION)
	cp -R FAQ LEGACY TODO LICENSE Makefile README config.mk\
		st.info st.1 arg.h st.h win.h $(SRC)\
		st-edit st-url st.desktop\
		st-mira-$(VERSION)
	cp config.def.h st-mira-$(VERSION)/config.def.h
	tar -cf - st-mira-$(VERSION) | gzip -f > st-mira-$(VERSION).tar.gz
	rm -rf st-mira-$(VERSION)

install: st
	mkdir -p $(DESTDIR)$(PREFIX)/bin
	@echo
	cp -f st $(DESTDIR)$(PREFIX)/bin
	cp -f st-url $(DESTDIR)$(PREFIX)/bin
	cp -f st-edit $(DESTDIR)$(PREFIX)/bin
	@echo
	chmod 755 $(DESTDIR)$(PREFIX)/bin/st
	chmod 755 $(DESTDIR)$(PREFIX)/bin/st-url
	chmod 755 $(DESTDIR)$(PREFIX)/bin/st-edit
	@echo
	mkdir -p $(DESTDIR)$(MANPREFIX)/man1
	sed "s/VERSION/$(VERSION)/g" < st.1 > $(DESTDIR)$(MANPREFIX)/man1/st.1
	chmod 644 $(DESTDIR)$(MANPREFIX)/man1/st.1
	@echo
	mkdir -p $(DESTDIR)$(PREFIX)/share/applications
	cp -f st.desktop $(DESTDIR)$(PREFIX)/share/applications
	@echo
	tic -sx st.info
	@echo
	@echo Please see the README file regarding the terminfo entry of st.
	@echo

uninstall:
	rm -f $(DESTDIR)$(PREFIX)/bin/st
	rm -f $(DESTDIR)$(PREFIX)/bin/st-url
	rm -f $(DESTDIR)$(PREFIX)/bin/st-edit
	rm -f $(DESTDIR)$(MANPREFIX)/man1/st.1
	rm -f $(DESTDIR)$(PREFIX)/share/applications/st.desktop

.PHONY: all options clean dist install uninstall
