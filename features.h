#ifndef __ST_FONT_FEATURES_H
#define __ST_FONT_FEATURES_H

#include <harfbuzz/hb.h>

#define ENABLE(c1,c2,c3,c4)             \
{                                       \
    .tag = HB_TAG(c1,c2,c3,c4),         \
    .value = 1,                         \
    .start = HB_FEATURE_GLOBAL_START,   \
    .end = HB_FEATURE_GLOBAL_END        \
}

/*
 * Poplulate the array with a list of font features, wrapped in ENABLE macro,
 * e. g.
 * ENABLE('c', 'a', 'l', 't'), ENABLE('d', 'l', 'i', 'g')
 */
static const hb_feature_t features[] = {
    ENABLE('s', 's', '0', '5'), /* at sign '@' */
    ENABLE('s', 's', '0', '6'), /* faded backslash \n, \\, etc */
};

#undef ENABLE
#endif /* !__ST_FONT_FEATURES_H */
