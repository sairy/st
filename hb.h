#include <X11/Xft/Xft.h>
#include <harfbuzz/hb.h>
#include <harfbuzz/hb-ft.h>

void hbunloadfonts(void);
void hbtransform(XftGlyphFontSpec *, const Glyph *, size_t, int, int);
